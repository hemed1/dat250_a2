package entities;


import ejbs.DeviceDao;
import ejbs.VoteDao;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

public class DeviceTest {

    private DeviceDao deviceFacade;
    private VoteDao voteFacade;

    @Before
    public void setUp() {
      deviceFacade = DeviceDao.of("projectUnit");
      voteFacade = VoteDao.of("projectUnit");
    }

    @Test
    public void testIfProjectUnitExists() {
        assertNotNull(deviceFacade);
    }

    @Test
    public void findVotefromDevice() {
        Device p = deviceFacade.find((long)1);
        assertNotNull(p);
        Vote v = voteFacade.createVote(5, p);
        assertNotNull(v);
        List<Vote> votes = p.getVotes();
        assertNotNull(votes);
        assertTrue(votes.contains(v));
    }

    @Test
    public void testFindingAllDevices() {
        List<Device> devices = deviceFacade.findAll();
        assertNotNull(devices);
        assertTrue(devices.size() > 0);
    }

}



