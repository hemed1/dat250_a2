package entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;


@XmlRootElement
@Entity
@Table(name = "vote", schema = "public")
public class Vote implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private int score;

    @ManyToOne
    @JoinColumn(name = "projectid")
    @JsonManagedReference
    private Device device;

    @Column(name = "ipadress")
    private String ip;

    public long getId() {
        return id;
    }

    @Basic
    @Column(name = "score")
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ipadress) {
        this.ip = ipadress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return id == vote.id &&
                Objects.equals(score, vote.score) &&
                Objects.equals(device, vote.device) &&
                Objects.equals(ip, vote.ip);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, score, ip);
    }

    @Override
    public String toString() {
        return "id: " + id + " score: " + score + " project_id: " + device.getId();
    }
}
