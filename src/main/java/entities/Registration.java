package entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;


@XmlRootElement
@Entity
@Table(name = "registration", schema = "public")
public class Registration implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonProperty("date_registered")
    private Date dateregistered;

    @ManyToOne
    @JoinColumn(name = "deviceid")
    @JsonManagedReference
    @JsonIgnore
    private Device device;

    @ManyToOne
    @JoinColumn(name = "userid")
    @JsonManagedReference
    @JsonProperty("subscriber")
    private User user;

    private boolean approved;

    public Registration() {
    }

    public Registration(Device device, User subscriber) {
        this.device = device;
        this.user = subscriber;
    }

    public long getId() {
        return id;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public String toString() {
        return "id: " + id + " device_id: " + device.getId();
    }

    public Date getDateregistered() {
        return dateregistered;
    }

    public void setDateregistered(Date registeredDate) {
        this.dateregistered = registeredDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
