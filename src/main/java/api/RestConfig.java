package api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * A class that register REST application path
 */

@ApplicationPath("rest")
public class RestConfig extends Application {
}
