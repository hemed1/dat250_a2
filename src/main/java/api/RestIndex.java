package api;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * Root for REST endpoint that will print HTML
 *
 * @author Hemed
 */

@Path("/")
public class RestIndex extends RestConfig {

    /**
     * Return html page with information about REST api.
     *
     *
     * @param servletContext Context of the servlet container.
     * @return HTML page which has information about all methods of REST API.
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String sayHtmlHello(@Context ServletContext servletContext) {
        return "<html>"
                + "<title>RESTful API </title>" +
                    "<body>" +
                        "<h2>REST endpoint is up and running</h2>" +
                        "REST path: " + servletContext.getContextPath() + "/rest" +
                        "<ul>" +
                        "<li>GET <a href='#'>/rest</a> - Returns this page.</li>" +
                        "</ul>" +
                    "</body>" +
                "</html>";
    }
}
