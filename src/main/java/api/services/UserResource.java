package api.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import ejbs.UserDao;
import entities.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;


@Path("/users")
@Stateless
public class UserResource {
    @EJB
    UserDao userDao;
    private Logger logger = Logger.getLogger(getClass().getName());
    private ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

    @GET
    @Produces( MediaType.APPLICATION_JSON)
    @Consumes( MediaType.APPLICATION_JSON)
    public Response getUsers() {
        List<User> users = userDao.findAll();
        if (Objects.nonNull(users)) {
            return Response
                    .ok(users)
                    .build();
        }
        return Response.noContent().build();
    }


    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUserById(@PathParam("id") String id) throws JsonProcessingException {
        User user = userDao.find(Long.parseLong(id));
        if (Objects.nonNull(user)) {
            return Response
                    .ok(mapper.writeValueAsString(user))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .build();
        }
        return Response.status(404).build();
    }

}
