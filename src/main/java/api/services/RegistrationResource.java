package api.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import ejbs.DeviceDao;
import ejbs.RegistrationDao;
import ejbs.UserDao;
import entities.Registration;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;


@Path("/registrations")
public class RegistrationResource {
    @EJB
    RegistrationDao registrationDao;
    @EJB
    DeviceDao deviceDao;
    @EJB
    UserDao userDao;

    private Logger logger = Logger.getLogger(getClass().getName());
    private ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

    @GET
    @Produces({MediaType.APPLICATION_XML})
    public Response getRegistrations() throws JsonProcessingException {
        List<Registration> registrations = registrationDao.findAll();
        if (Objects.nonNull(registrations)) {
            return Response
                    .ok(mapper.writeValueAsString(registrations))
                    //.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .build();
        }
        return Response.noContent().build();
    }

    /**
     * Gets registration by a given identifier
     */
    @GET
    @Path("/{id}")
    public Response getRegistrationById(@PathParam("id") String id) throws JsonProcessingException {
        Registration registration = registrationDao.find(Long.parseLong(id));
        if (Objects.nonNull(registration)) {
            return Response
                    .ok(mapper.writeValueAsString(registration))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .build();
        }
        return Response.status(404).build();
    }

}
