package api.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import ejbs.DeviceDao;
import ejbs.RegistrationDao;
import ejbs.UserDao;
import entities.Device;
import entities.Registration;
import entities.User;
import entities.Vote;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

@Path("/devices")
public class DeviceResource {
    @EJB
    DeviceDao deviceDao;
    @EJB
    RegistrationDao registrationDao;
    @EJB
    UserDao userDao;

    private Logger logger = Logger.getLogger(getClass().getName());
    private ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

    /**
     * Gets all devices in the database and show them as JSON string
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getDevices() throws JsonProcessingException {
        List<Device> devices = deviceDao.findAll();
        if (Objects.nonNull(devices)) {
            return Response
                    .ok(mapper.writeValueAsString(devices))
                    .build();
        }
        return Response.noContent().build();
    }

    /**
     * Gets device by a given Id
     */
    @GET
    @Path("/{id}")
    public Response getDeviceById(@PathParam("id") String id) throws JsonProcessingException {
        Device device = deviceDao.find(Long.parseLong(id));
        if (Objects.nonNull(device)) {
            return Response
                    .ok(mapper.writeValueAsString(device))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .build();
        }
        return Response.status(404).build();
    }


    /**
     * Gets device by a given Id
     */
    @GET
    @Path("/{id}/registrations")
    public Response getRegistrationsByDeviceId(@PathParam("id") String id) throws JsonProcessingException {
        Device device = deviceDao.find(Long.parseLong(id));
        if (Objects.nonNull(device)) {
            List<entities.Registration> registrations = device.getRegistrations();
            return Response
                    .ok(mapper.writeValueAsString(registrations))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .build();
        }
        return Response.status(404).build();
    }


    @GET
    @Path("/{id}/registrations/{rid}")
    public Response getRegistrationByDeviceId(@PathParam("id") String id, @PathParam("id") String rid)
            throws JsonProcessingException {
        Device device = deviceDao.find(Long.parseLong(id));
        if (Objects.nonNull(device)) {
            Registration reg = null;
            List<entities.Registration> registrations = device.getRegistrations();

            for (Registration registration : registrations) {
                if (registration.getId() == Long.parseLong(rid)) {
                    reg = registration;
                    break;
                }
            }
            return Response
                    .ok(mapper.writeValueAsString(reg))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .build();
        }
        return Response.status(404).build();
    }

    /**
     * Creates device by name and gives response if operation was successful
     */
    @POST
    @Path("/{id}/registration")
    @QueryParam("userId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createRegistration(@PathParam("id") String deviceId, @QueryParam("userId") String userId)
            throws JsonProcessingException {
        Device device = deviceDao.find(Long.parseLong(deviceId));
        User user = userDao.find(Long.parseLong(userId));

        if (device != null && user != null) {
            Registration registration = registrationDao.createRegistration(device, user);
            //Create response content
            ObjectNode bucket = mapper.createObjectNode()
                    .put("created", true)
                    .putPOJO("registration", registration);

            logger.info("New registration has been created: " + registration);
            return Response
                    .ok(mapper.writeValueAsString(bucket))
                    .build();
        }
        return Response.status(404).build();

    }

    /**
     * Creates device by name and gives response if operation was successful
     */
    @POST
    @Path("/{name}")
    @QueryParam("ownerId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createDevice(@PathParam("name") String name, @QueryParam("ownerId") String ownerId)
            throws JsonProcessingException {

        User owner = userDao.find(Long.parseLong(ownerId));
        Device device = deviceDao.createDevice(name, owner);
        //Create response content
        ObjectNode bucket = mapper.createObjectNode()
                .put("created", true)
                .putPOJO("device", device);

        logger.info("New device was created: " + device);
        return Response
                .ok(mapper.writeValueAsString(bucket))
                .build();
    }


    @GET
    @Path("/{id}/votes")
    public Response getVotesByDeviceId(@PathParam("id") String id) throws JsonProcessingException {
        Device device = deviceDao.find(Long.parseLong(id));
        if (Objects.nonNull(device)) {
            List<Vote> votes = device.getVotes();
            return Response
                    .ok(mapper.writeValueAsString(votes))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .build();
        }
        return Response.status(404).build();
    }

}
