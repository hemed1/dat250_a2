package api.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import ejbs.VoteDao;
import entities.Vote;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * Endpoint for votes
 */
@Path("/votes")
public class VoteResource {
    @EJB
    VoteDao voteDao;
    private Logger logger = Logger.getLogger(getClass().getName());
    private ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

    /**
     * Gets all projects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVotes() throws JsonProcessingException {
        List<Vote> votes = voteDao.findAll();
        if (Objects.nonNull(votes)) {
            return Response
                    .ok(mapper.writeValueAsString(votes))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .build();
        }
        return Response.noContent().build();
    }

    /**
     * Gets vote by a given identifier
     */
    @GET
    @Path("/{id}")
    public Response getVoteById(@PathParam("id") String id) throws JsonProcessingException {
        Vote vote = voteDao.find(Long.parseLong(id));
        if (Objects.nonNull(vote)) {
            return Response
                    .ok(mapper.writeValueAsString(vote))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .build();
        }
        return Response.status(404).build();
    }
}
