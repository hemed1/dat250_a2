package jms;

import com.google.gson.JsonObject;
import entities.Registration;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.io.IOException;

@MessageDriven(mappedName = "jms/dat250/Topic", activationConfig = {
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "topicSubscription = 'dweet'") })
public class SubscriptionListener implements MessageListener {
    @Override
    public void onMessage(Message message) {
        try {
            Registration subscription = message.getBody(Registration.class);
            JsonObject json = new JsonObject();
            json.addProperty("deviceId", subscription.getDevice().getId());
            json.addProperty("deviceName", subscription.getDevice().getName());
            json.addProperty("subscriberUsername", subscription.getUser().getUsername());
            new SubscriptionConnection().publish(json);
        } catch (JMSException | IOException e) {
            e.printStackTrace();
        }
    }
}
