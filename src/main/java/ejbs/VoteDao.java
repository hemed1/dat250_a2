package ejbs;

import entities.Device;
import entities.Vote;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;

/**
 * A class where users can vote for a particular device to provide feedback to the owner
 */
@Stateless
public class VoteDao extends AbstractFacade<Vote> {
    private EntityManager entityManager;

    private VoteDao() {
        super(Vote.class);
        entityManager = createEntityManager(PROJECT_UNIT_NAME);
    }

    private VoteDao(String name) {
        super(Vote.class);
        entityManager = createEntityManager(name);
    }

    public static VoteDao of(String unitName) {
        return new VoteDao(unitName);
    }

    public void merge(Vote v) {
        entityManager.merge(v);
    }

    public void create(Vote entity) {
        getEntityManager().persist(entity);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }


    public Vote createVote(int score, Device device) {
        entityManager.getTransaction().begin();
        Vote v = new Vote();
        v.setScore(score);
        v.setDevice(device);
        create(v);
        entityManager.getTransaction().commit();
        return v;
    }
}
