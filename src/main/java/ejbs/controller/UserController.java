package ejbs.controller;

import ejbs.UserDao;
import entities.Device;
import entities.User;
import utils.Constants;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Named(value = "userController")
@RequestScoped
public class UserController implements Serializable {

    private static final long serialVersionUID = 1L;
    @EJB
    private UserDao userDao;

    private User user;
    private Device device;


    public List<User> getUsers() {
        List<User> reverseDeviceList = new ArrayList<>(this.userDao.findAll());
        Collections.reverse(reverseDeviceList);
        return reverseDeviceList;
    }

    public String register() {
        if (userDao.userExists(user.getUsername())) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            "User with this username already exists",
                            null)
            );
            return Constants.REGISTER;
        }
        userDao.createUser(this.user.getName(), this.user.getUsername(), this.user.getPassword());
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(
                        FacesMessage.SEVERITY_ERROR,
                        "User has been successfully registered",
                        null)
        );
        return Constants.LOGIN;
    }

    public User getUser() {
        if (this.user == null) {
            user = new User();
        }
        return user;
    }

    public Device getDevice() {
        if (this.device == null) {
            device = new Device();
        }
        return device;
    }

}
