package ejbs.controller;

import ejbs.DeviceDao;
import ejbs.UserDao;
import entities.Device;
import entities.Registration;
import utils.Constants;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named(value = "deviceController")
@SessionScoped
public class DeviceController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private DeviceDao deviceDao;

    @EJB
    private UserDao userDao;

    private Device device;


    public void saveDevice() {
        this.deviceDao.create(this.device);
    }

    public List<Device> getAllDevices() {
        return deviceDao.findAll();
    }


    public String mapStatusToCharacter(String status) {
        if (status.equals("ONLINE") || status.equals("AVAILABLE"))
            return "●";

        return "✕";
    }

    public Device showDevice(long deviceId) {
        return deviceDao.find(deviceId);
    }


    public String viewDevice(long deviceId) {
        try {
            this.device = this.deviceDao.find(deviceId);
        } catch (Exception e) {
            return Constants.DEVICES;
        }

        return Constants.DEVICE;
    }

    public List<Device> getUserDevices(long userId) {
        return userDao.getUserDevices(userId);
    }

    public List<Registration> getRegistrations(long deviceId) {
        Device device = deviceDao.find(deviceId);
        return device.getRegistrations();
    }

}



