package ejbs.controller;

import ejbs.UserDao;
import entities.User;
import utils.Constants;
import utils.SessionUtil;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.security.Principal;
import java.util.Map;

@Named(value = "sessionController")
@SessionScoped
public class SessionController implements Serializable {

    private static final long serialVersionUID = 1L;

    private String password;
    private String username;
    private User user;

    @EJB
    private UserDao userDao;

    public String login() throws ServletException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
           request.login(username, password);
            //return Constants.LOGIN;
        }
        catch (ServletException e) {
            context.addMessage(null,
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            "Wrong username nad/or password: " + e.getLocalizedMessage(),
                            null)
            );
            return Constants.LOGIN;
        }
        Principal principal = request.getUserPrincipal();
        this.user = userDao.getUser(principal.getName());
        Map<String, Object> sessionMap = SessionUtil.getSessionMap();
        sessionMap.put(Constants.USER, user);
        SessionUtil.getSession().setAttribute(Constants.USERNAME, this.username);
        return Constants.DEVICES;

    }

    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = SessionUtil.getRequest();
        try {
            request.logout();
            this.user = null;
            SessionUtil.getSession().invalidate();
        } catch (ServletException ignored) {
        }
        return Constants.LOGIN;
    }

    public User getUser(){
        return (User)SessionUtil.getSessionMap().get(Constants.USER);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
