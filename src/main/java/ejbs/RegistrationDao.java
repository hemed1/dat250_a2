package ejbs;

import entities.Device;
import entities.Registration;
import entities.User;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSSessionMode;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import java.util.Date;

@Stateless
public class RegistrationDao extends AbstractFacade<Registration> {
    private EntityManager entityManager;

    @Inject
    @JMSConnectionFactory("jms/dat250/ConnectionFactory")
    @JMSSessionMode(JMSContext.AUTO_ACKNOWLEDGE)
    private JMSContext context;

    @Resource(lookup = "jms/dat250/Topic")
    private Topic topic;


    public void approveSubscription(Registration subscription) {
        subscription.setApproved(true);
        entityManager.merge(subscription);
        context.createProducer()
                .setProperty("topicSubscription", "dweet")
                .send(topic, subscription);
    }

    private RegistrationDao() {
        super(Registration.class);
        entityManager = createEntityManager(PROJECT_UNIT_NAME);
    }

    private RegistrationDao(String name) {
        super(Registration.class);
        entityManager = createEntityManager(name);
    }

    public void create(Registration entity) {
        getEntityManager().persist(entity);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }


    /**
     * Creates registration and adds it to the database
     */
    public Registration createRegistration(Device device, User user) {
        entityManager.getTransaction().begin();
        Registration reg = new Registration(device, user);
        reg.setDateregistered(new Date());
        create(reg);
        entityManager.getTransaction().commit();
        return reg;
    }
}
