package ejbs;


import entities.Device;
import entities.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.ws.rs.NotFoundException;

@Stateless
public class DeviceDao extends AbstractFacade<Device> {
    private EntityManager entityManager;

    private DeviceDao() {
        super(Device.class);
        entityManager = createEntityManager(PROJECT_UNIT_NAME);
    }

    private DeviceDao(String name) {
        super(Device.class);
        entityManager = createEntityManager(name);
    }

    public static DeviceDao of(String unitName) {
        return new DeviceDao(unitName);
    }

    public void merge(Device p) {
        entityManager.merge(p);
    }

    public void create(Device entity) {
        getEntityManager().persist(entity);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }


    public Device createDevice(String name, User owner) {
        entityManager.getTransaction().begin();
        if (owner != null) {
            Device device = new Device(name, owner);
            create(device);
            entityManager.getTransaction().commit();
            return device;
        }
        throw new NotFoundException("Cannot create device");
    }

}
