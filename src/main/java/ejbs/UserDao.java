package ejbs;

import entities.Device;
import entities.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Objects;

@Stateless
public class UserDao extends AbstractFacade<User> {
    private EntityManager entityManager;

    private UserDao() {
        super(User.class);
        entityManager = createEntityManager(PROJECT_UNIT_NAME);
    }

    private UserDao(String name) {
        super(User.class);
        entityManager = createEntityManager(name);
    }


    public void create(User entity) {
        getEntityManager().persist(entity);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }


    public User createUser(String name, String username, String password) {
        entityManager.getTransaction().begin();
        User user = new User(name, username, password);
        this.create(user);
        entityManager.getTransaction().commit();
        return user;
    }

    public boolean userExists(String username) {
        return Objects.nonNull(this.getUser(username));
    }


    public User getUser(String username) {
        TypedQuery<User> q = entityManager.createQuery("select user from User user where user.username=?1", User.class);
        q.setParameter(1, username);
        List<User> users = q.getResultList();
        if (users.isEmpty())  {
            return null;
        }
        return users.get(0);
    }


    public List<Device> getUserDevices(String userId) {
        TypedQuery<Device> q = entityManager.createQuery("select device from Device device where device.user.id=?1", Device.class);
      return q.getResultList();

    }

    public List<Device> getUserDevices(long userId) {
        User user = find(userId);
        return user.getDevices();
    }
}
