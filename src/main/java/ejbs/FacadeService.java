package ejbs;

import java.util.List;

/**
 * A contract for defining Facade services
 *
 * @param <T>
 */
interface  FacadeService<T> {
    /**
     * Creates entity
     */
    void create(T entity);

    /**
     * Edits the given entity
     */
    void edit(T entity);

    /**
     * Removes the given entity
     */
    void remove(T entity);

    /**
     * Finds the entity in the database
     */
    T find(Object id) ;

    /**
     * Finds all entities
     */
    List<T> findAll() ;

    /**
     * Counts the number of entities
     */
    int count();


}
